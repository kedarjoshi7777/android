/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.android.bluetoothlegatt;

import android.app.Activity;
import android.app.ListActivity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.ScanRecord;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.os.Parcel;
import android.os.Parcelable;
import android.provider.ContactsContract;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.le.ScanResult;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.FileWriter;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;

/**
 * Activity for scanning and displaying available Bluetooth LE devices.
 */
public class DeviceScanActivity extends Activity {
    private LeDeviceListAdapter mLeDeviceListAdapter;
    private BluetoothAdapter mBluetoothAdapter;
    private boolean mScanning;
    private Handler mHandler;
    private Handler gHandler;
    private Handler sendHandler;
    private int rssi1 = 0;
    private EditText location_x;
    private EditText location_y;
    private EditText shop_name;
    private EditText floor;
    private EditText property;
    private EditText section;
    private String shop_s;
    private int floor_s;
    private String section_s;
    private String property_s;
    private int x_location;
    private int y_location;
    private double[] Coords = new double[2];
    private boolean flag = true;
    private long time_s;
    private double[] gps = new double[2];
    protected String currentLatitude, currentLongitude;
    double lat, lon;
    protected LocationManager locationManager;
    private LocationListener location_listener;
    private String Address ;
    private String IMEI;
    private String ShortHash;
    private String Hash;
    private String Phone;
    private String send_Hash;
    private boolean change = false;
    private static final int REQUEST_ENABLE_BT = 1;
    // Stops scanning after 10 seconds.
    private static final long SCAN_PERIOD = 2000000000;
    private static final long TIME_PERIOD_HERE = 10000;
    private static  final long TIME_PERIOD_GPS = 10000;
    static final char[] hexArray = "0123456789ABCDEF".toCharArray();
    private String uuid;
    private int major;
    private int minor;
    JSONObject returnObj = new JSONObject();
    JSONObject RSSI = new JSONObject();
    JSONObject BAAP = new JSONObject();


    private int count = 0;
    HashMap<String, ArrayList<Integer>> map = new HashMap<String, ArrayList<Integer>>();
    ArrayList<ArrayList<Integer>> val = new ArrayList<ArrayList<Integer>>();

    ExecutorService executorService = Executors.newSingleThreadExecutor();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.listitem_device);


        getActionBar().setTitle(R.string.title_devices);

        mHandler = new Handler(){
            @Override
            public void handleMessage(Message msg) {
                if(msg.what == 1 && flag) {

                    executorService.submit(thread);
                    Log.v(" I am inside ", " handler message ");
                    //executorService.shutdown();

                    mHandler.sendEmptyMessageDelayed(1, TIME_PERIOD_HERE);


                } else {
                    //TODO: end scan here
                    Log.v("In the ", "else loop of handler");
                    mScanning = false;
                    mBluetoothAdapter.stopLeScan(mLeScanCallback);
                    invalidateOptionsMenu();
                    map = new HashMap<String,ArrayList<Integer>>();
                    val = new ArrayList<ArrayList<Integer>>();
                    count = 0;
                }
            }

        };
        gHandler = new Handler(){
            @Override
            public void handleMessage(Message msg){
                if(msg.what == 3){
                    executorService.submit(coordinate);
                    gHandler.sendEmptyMessageDelayed(3, TIME_PERIOD_GPS);
                }
            }
        };
        location_x = (EditText) findViewById(R.id.location_x);
        location_y = (EditText) findViewById(R.id.location_y);
        shop_name = (EditText) findViewById(R.id.shop_name);
        floor = (EditText) findViewById(R.id.floor);
        property = (EditText) findViewById(R.id.property);
        section = (EditText) findViewById(R.id.section);
        location_x.setText("0");
        location_y.setText("0");
        // Use this check to determine whether BLE is supported on the device.  Then you can
        // selectively disable BLE-related features.
        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Toast.makeText(this, R.string.ble_not_supported, Toast.LENGTH_SHORT).show();
            finish();
        }

        // Initializes a Bluetooth adapter.  For API level 18 and above, get a reference to
        // BluetoothAdapter through BluetoothManager.
        final BluetoothManager bluetoothManager =
                (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();

        // Checks if Bluetooth is supported on the device.
        if (mBluetoothAdapter == null) {
            Toast.makeText(this, R.string.error_bluetooth_not_supported, Toast.LENGTH_SHORT).show();
            finish();
            return;
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        if (!mScanning) {
            menu.findItem(R.id.menu_stop).setVisible(false);
            menu.findItem(R.id.menu_scan).setVisible(true);
            menu.findItem(R.id.menu_refresh).setActionView(null);
        } else {
            menu.findItem(R.id.menu_stop).setVisible(true);
            menu.findItem(R.id.menu_scan).setVisible(false);
            menu.findItem(R.id.menu_refresh).setActionView(
                    R.layout.actionbar_indeterminate_progress);
        }
        return true;
    }
    public String getHash(){
        /****
         * Calculates Hash
         ****/
        // Earlier it was implemented using if block, why was the required?
        TelephonyManager telephonyManager = (TelephonyManager) this.getApplicationContext()
                .getSystemService(Context.TELEPHONY_SERVICE);
        Log.v("after","telephone");
        IMEI = "123";
        Phone = "970957";
        Log.v("after","IMEI");

        try {
            Hash = Functions.computeHash(IMEI + Phone);
            //Log.i("Our Hash", Hash);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        ShortHash = Hash.substring(0, 10);
        /**********************/
        Log.v("In","getHash()");
        return ShortHash;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_scan:
                mLeDeviceListAdapter.clear();
                scanLeDevice(true);
                break;
            case R.id.menu_stop:
                scanLeDevice(false);
                break;
        }
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();

        // Ensures Bluetooth is enabled on the device.  If Bluetooth is not currently enabled,
        // fire an intent to display a dialog asking the user to grant permission to enable it.
        if (!mBluetoothAdapter.isEnabled()) {
            if (!mBluetoothAdapter.isEnabled()) {
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            }
        }

        // Initializes list view adapter.
        mLeDeviceListAdapter = new LeDeviceListAdapter();
        //setListAdapter(mLeDeviceListAdapter);
        scanLeDevice(true);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // User chose not to enable Bluetooth.
        if (requestCode == REQUEST_ENABLE_BT && resultCode == Activity.RESULT_CANCELED) {
            finish();
            return;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onPause() {
        super.onPause();
        scanLeDevice(false);
        mLeDeviceListAdapter.clear();
    }

   // @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        final BluetoothDevice device = mLeDeviceListAdapter.getDevice(position);
        if (device == null) return;
        final Intent intent = new Intent(this, DeviceControlActivity.class);
        intent.putExtra(DeviceControlActivity.EXTRAS_DEVICE_NAME, device.getName());
        intent.putExtra(DeviceControlActivity.EXTRAS_DEVICE_ADDRESS, device.getAddress());
        intent.putExtra(DeviceControlActivity.EXTRAS_DEVICE_RSSI, rssi1);

        if (mScanning) {
            mBluetoothAdapter.stopLeScan(mLeScanCallback);
            mScanning = false;
        }
        startActivity(intent);
    }

    private Semaphore semaphore = new Semaphore(1);
    Runnable thread = new Runnable(){

        @Override
        public void run() {
            //HttpClient httpclient = new DefaultHttpClient();
            Log.v("Before","connection");
            HttpPost httppost = new HttpPost("http://192.168.0.8:13234");
            Log.v("After","connection");
            send_Hash = getHash();
            RSSI = new JSONObject(map);
            Log.v("After","getHash()");
            try{

                JSONArray gps_array = new JSONArray();
                gps_array.put(gps[0]);
                gps_array.put(gps[1]);

                Log.v("In the","Loop of thread");
                BAAP.put("info", RSSI);
                BAAP.put("shop", shop_s);
                BAAP.put("property", property_s);
                BAAP.put("section", section_s);
                BAAP.put("floor", floor_s);
                BAAP.put("x", x_location);
                BAAP.put("y", y_location);
                BAAP.put("timestamp", time_s );
                BAAP.put("gps", gps_array);
                BAAP.put("hash", send_Hash);
            }
            catch(Exception ex){
                ex.printStackTrace();
            }
            HttpParams http_parameters = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(http_parameters, 10000);
            HttpConnectionParams.setSoTimeout(http_parameters, 10000);
            InputStream input_stream;
            HttpClient http_client = new DefaultHttpClient(http_parameters);
            try {
                Log.v("Value", BAAP.toString());
                httppost.setEntity(new StringEntity(BAAP.toString() + "END"));
                HttpResponse response = http_client.execute(httppost);
            }
            catch (Exception ex){
                ex.printStackTrace();
            }
            map = new HashMap<String,ArrayList<Integer>>();
            val = new ArrayList<ArrayList<Integer>>();
            count = 0;
        }
    };
    Runnable coordinate = new Runnable() {
        @Override
        public void run() {

            getGPS();
            try{
                gps = Coords;

            }catch (Exception e){
                e.printStackTrace();
            }
        }
    };
    private void scanLeDevice(final boolean enable) {
        if (enable) {
            // Stops scanning after a pre-defined scan period.

            mHandler.sendEmptyMessageDelayed(0, SCAN_PERIOD);
            Log.v("Spiderman", " Batman");
            mHandler.sendEmptyMessageDelayed(1, TIME_PERIOD_HERE);
            gHandler.sendEmptyMessageDelayed(3, TIME_PERIOD_GPS);

            mScanning = true;
            mBluetoothAdapter.startLeScan(mLeScanCallback);
            flag = true;
            //System.out.println(myMap);
        } else {
            mScanning = false;
            mBluetoothAdapter.stopLeScan(mLeScanCallback);

            flag = false;
        }
        invalidateOptionsMenu();
    }


    private BluetoothAdapter.LeScanCallback mLeScanCallback =
            new BluetoothAdapter.LeScanCallback() {

                @Override
                public void onLeScan(final BluetoothDevice device, final int rssi, final byte[] scanRecord) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            int startByte = 2;
                            boolean patternFound = false;
                            while (startByte <= 5) {
                                if (    ((int) scanRecord[startByte + 2] & 0xff) == 0x02 && //Identifies an iBeacon
                                        ((int) scanRecord[startByte + 3] & 0xff) == 0x15) { //Identifies correct data length
                                    patternFound = true;
                                    break;
                                }
                                startByte++;
                            }

                            if (patternFound) {
                                //Convert to hex String
                                byte[] uuidBytes = new byte[16];
                                System.arraycopy(scanRecord, startByte+4, uuidBytes, 0, 16);
                                String hexString = bytesToHex(uuidBytes);

                                //Here is your UUID
                                uuid =  hexString.substring(0,8) + "-" +
                                        hexString.substring(8,12) + "-" +
                                        hexString.substring(12,16) + "-" +
                                        hexString.substring(16,20) + "-" +
                                        hexString.substring(20,32);

                                //Here is your Major value
                                major = (scanRecord[startByte+20] & 0xff) * 0x100 + (scanRecord[startByte+21] & 0xff);

                                //Here is your Minor value
                                minor = (scanRecord[startByte+22] & 0xff) * 0x100 + (scanRecord[startByte+23] & 0xff);
                            }
                            time_s = System.currentTimeMillis();
                            x_location = Integer.getInteger(location_x.getText().toString(),4);
                            y_location = Integer.getInteger(location_y.getText().toString(),5);

                            property_s = property.getText().toString();
                            section_s = section.getText().toString();
                            floor_s = Integer.getInteger(floor.getText().toString(), -1);
                            shop_s = shop_name.getText().toString();
                            try {

                                 addProperty(returnObj, uuid.toString(), rssi);
                                }

                            catch(Exception e){
                                e.printStackTrace();
                            }
                            mLeDeviceListAdapter.addDevice(device);
                            mLeDeviceListAdapter.notifyDataSetChanged();
                            }
                    });

                }
            };

    // Adapter for holding devices found through scanning.
    private class LeDeviceListAdapter extends BaseAdapter {
        private ArrayList<BluetoothDevice> mLeDevices;
        private LayoutInflater mInflator;

        public LeDeviceListAdapter() {
            super();
            mLeDevices = new ArrayList<BluetoothDevice>();
            mInflator = DeviceScanActivity.this.getLayoutInflater();
        }

        public void addDevice(BluetoothDevice device) {
            if (!mLeDevices.contains(device)) {
                mLeDevices.add(device);
            }
        }

        public BluetoothDevice getDevice(int position) {
            return mLeDevices.get(position);
        }

        public void clear() {
            mLeDevices.clear();
        }

        @Override
        public int getCount() {
            return mLeDevices.size();
        }

        @Override
        public Object getItem(int i) {
            return mLeDevices.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            ViewHolder viewHolder;
            // General ListView optimization code.
            if (view == null) {
                view = mInflator.inflate(R.layout.listitem_device, null);
                viewHolder = new ViewHolder();
                viewHolder.deviceAddress = (TextView) view.findViewById(R.id.device_address);
                viewHolder.deviceName = (TextView) view.findViewById(R.id.device_name);
                viewHolder.deviceRssi = (TextView) view.findViewById(R.id.device_rssi);
                view.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) view.getTag();
            }

            BluetoothDevice device = mLeDevices.get(i);
            final String deviceName = device.getName();
            //final int deviceRssi =
            //final String rssi = device.action.FOUND;
            //final String kedar = device.mRssi;

            if (deviceName != null && deviceName.length() > 0) {
                viewHolder.deviceName.setText(deviceName);
                if (change) {
                    viewHolder.deviceRssi.setText(rssi1);
                    change = false;
                }

            } else {
                viewHolder.deviceName.setText(R.string.unknown_device);
                viewHolder.deviceAddress.setText(device.getAddress());

            }
            return view;
        }


    }

    private void addProperty(JSONObject obj, String addr, int value)
    {
        //Believe exception only occurs when adding duplicate keys, so just ignore it
        try
        {   boolean flag = false;

            obj.put(addr, value);
            flag = map.containsKey(addr);
            if(flag){
                int z = map.get(addr).size();
                map.get(addr).add(z,value);
                flag = true;
                Log.v("Inside if loop",map.get(addr).toString());
            }
            else{
                ArrayList<Integer> nal = new ArrayList<Integer>();
                nal.add(0,value);
                val.add(count,nal);
                map.put(addr, val.get(count));
                count = count + 1;
            }
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
        /*
        try{
            String FILE_NAME = "infi_mall_shop.txt";
            String baseDir = Environment.getExternalStorageDirectory().getAbsolutePath();
            File file1 = new File(baseDir,FILE_NAME);
            FileWriter file = new FileWriter(file1,true);
            file.append(obj.toString() + "\n");
            file.flush();
            file.close();
            Log.e("Successfully written",obj.toString());
        }
        catch (IOException e) {
            e.printStackTrace();
            Log.e("Message", obj.toString());
        }
        */

    }
    // Device scan callback.

    private static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for ( int j = 0; j < bytes.length; j++ ) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }
    static class ViewHolder {
        TextView deviceName;
        TextView deviceAddress;
        TextView deviceRssi;
    }



    /****
     * GPS Class Definition
     ****/

    protected void getGPS() {
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        // Try getting GPS
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            location_listener = new GpsListener();
            locationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER, 0, 0, location_listener);

        }
        // Else try getting Network
        else {

            if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                location_listener = new GpsListener();
                locationManager.requestLocationUpdates(
                        LocationManager.NETWORK_PROVIDER, 0, 0, location_listener);

            }
            else {
                Toast.makeText(this,
                        "Enable location settings to get current location.",
                        Toast.LENGTH_LONG).show();

            }
        }
    }

    /****
     * GPS Listener Class Definition
     ****/
    private class GpsListener implements LocationListener {

        @Override
        public void onLocationChanged(Location location) {
            // TODO Auto-generated method stub
            if (location != null) {
                try {
                    Log.i("GPSLocation", "Found");
                } catch (Exception e) {}

                Coords[0] = location.getLatitude();
                Coords[1] = location.getLongitude();


                try {
                    if (location_listener != null)
                        locationManager.removeUpdates(location_listener);
                } catch (Exception e) {}

                locationManager = null;
            } else {
                Log.i("Warning ", "No Location Found");
            }

        }

        /****
         * UnImplemented Methods
         ****/
        @Override
        public void onProviderDisabled(String provider) {
            // TODO Auto-generated method stub

        }

        @Override
        public void onProviderEnabled(String provider) {
            // TODO Auto-generated method stub

        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            // TODO Auto-generated method stub

        }
        /********************/

    }// end of Class




}
