package com.example.android.bluetoothlegatt;

import android.location.LocationListener;
import android.location.LocationManager;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by kedar on 22/5/15.
 */
public class Functions {
    /*
	 * GPS Class and Getgps function
	 */
    // Variables
    protected String currentLatitude, currentLongitude;
    protected LocationManager locationManager;
    private LocationListener ll;
    private final double[] gps = new double[2];

    // FUTURE WORK: Reduce this 32byte hash
    static protected String computeHash(String input) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        MessageDigest digest = MessageDigest.getInstance("SHA256");
        digest.reset();

        byte[] byteData = digest.digest(input.getBytes("UTF-8"));
        StringBuffer sb = new StringBuffer();

        for (int i = 0; i < byteData.length; i++) {
            sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
        }
        return sb.toString();
    }
}
